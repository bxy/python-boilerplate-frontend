const STATIC_ROUTES = [
  '',
  '/about',
  '/flask'
]

const SITEMAP_CONFIGS = [
  '/py2',
  '/py3',
  '/py2+executable',
  '/py3+executable',
  '/py2+executable+argparse+logging',
  '/py3+executable+argparse+logging',
  '/py2+executable+argparse+pytest',
  '/py3+executable+argparse+pytest',
  '/py2+executable+argparse+unittest',
  '/py3+executable+argparse+unittest',
  '/py2+executable+argparse+logging+pytest+tox',
  '/py3+executable+argparse+logging+pytest+tox',
  '/py2+executable+argparse+logging+unittest+tox',
  '/py3+executable+argparse+logging+unittest+tox',
  '/py2+flask',
  '/py3+flask',
  '/py2+flask+pytest',
  '/py3+flask+pytest',
  '/py2+flask+pytest+tox',
  '/py3+flask+pytest+tox',
  '/py2+flask+unittest+tox',
  '/py3+flask+unittest+tox',
  '/py2+flask+argparse+logging+pytest+tox',
  '/py3+flask+argparse+logging+pytest+tox',
  '/py2+flask+argparse+logging+unittest+tox',
  '/py3+flask+argparse+logging+unittest+tox'
]

module.exports = {
  STATIC_ROUTES,
  SITEMAP_CONFIGS
}

// cli mode
if (!module.parent) {
  const configs = SITEMAP_CONFIGS
  const routes = STATIC_ROUTES.concat(configs)

  let sitemap = `<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`

  const now = (new Date()).toISOString()

  for (let route of routes) {
    // console.log('adding', route)
    sitemap += `<url>
    <loc>https://www.python-boilerplate.com${route}</loc>
    <lastmod>${now}</lastmod>
  </url>`
  }

  sitemap += '\n</urlset>'
  console.log(sitemap)
}
